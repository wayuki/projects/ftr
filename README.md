# FTR
This application can be accessed via [https://ftr.wayuki.org](https://ftr.wayuki.org).

## Setup
How do I spin up FTR locally?

1. Clone this repository.
    ```PowerShell
    # Windows Powershell
    git clone git@gitlab.com:wayuki/projects/ftr.git
    cd .\ftr
    ```
    ```Shell
    # Unix Shell
    git clone git@gitlab.com:wayuki/projects/ftr.git
    cd ./ftr
    ```
2. Create a file `.env` with the content copied from `.env.example`, and fill out the values.
   1. `PUBLIC_URL` is the full URL which a browser can access FTR, e.g. `http://localhost:3000`.
   2. `PORT` is the port which FTR will be served in internally.

    <details>
      <summary markdown="span">Note</summary>

      Keep in mind that the port in `PORT` does not have to match the port in `PUBLIC_URL` if you are using Docker.

      For example, if you want FTR to be served on port `3000` inside the Docker container, but want to map the port to `3333` on the host machine, the `PORT` should be `3000` while `PUBLIC_URL` should be `http://localhost:3333`.
    </details>
3. [Only when using Docker]
   1. Create a file `docker-compose.override.yml` with the content copied from `docker-compose.override.yml.example`, and customize it to your liking.
   2. Run `docker-compose build base`
4. [Only when using Yarn on host] Run `yarn`.
5. Spin up FTR.
    ```Shell
    # Use Docker
    docker-compose up local
    # Use NodeJS on host
    node start
    ```
6. Visit `PUBLIC_URL` on your browser.
