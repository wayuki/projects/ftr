import App from 'components/App';
import Container from 'components/common/Container';
import React from 'react';
import ReactDOM from 'react-dom';

ReactDOM.render(
  <Container>
    <App />
  </Container>,
  document.querySelector('#app'),
);
