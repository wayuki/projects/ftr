const qs = {
  parse<R extends Record<string, any> = {}>(queryString: string): R {
    let search = queryString;
    if (!search) {
      return {} as R;
    }

    if (search.startsWith('?')) {
      search = search.slice(1);
    }

    return search.split('&')
      .reduce<R>(
      (acc, query) => {
        const [key, value] = query.split('=');

        (acc as any)[window.decodeURIComponent(key)] = window.decodeURIComponent(value);

        return acc;
      },
      {} as R,
    );
  },
  stringify<R extends Record<string, any>>(params: R) {
    const queryString = Object.entries(params)
      .filter(
        ([, value]) => (
          value !== null || value !== undefined
        ),
      )
      .map(
        ([key, value]) => (
          `${window.encodeURIComponent(key)}=${window.encodeURIComponent(value)}`
        ),
      )
      .join('&');

    if (queryString) {
      return `?${queryString}`;
    }

    return '';
  },
};

export {
  qs, // eslint-disable-line import/prefer-default-export
};
