import { createBrowserHistory } from 'history';
import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import Landing from './components/Landing';
import NotFoundPage from './components/NotFoundPage';
import NumberTracker from './components/NumberTracker';
import Offboarding from './components/Offboarding';

const history = createBrowserHistory();

const App = () => (
  <Router history={history}>
    <Switch>
      <Route path="/number-tracker" exact component={NumberTracker} />
      <Route path="/results" exact component={Offboarding} />
      <Route path="/" exact component={Landing} />
      <Route component={NotFoundPage} />
    </Switch>
  </Router>
);

export default App;
