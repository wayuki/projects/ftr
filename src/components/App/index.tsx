import React, { PureComponent } from 'react';
import * as uuid from 'uuid';
import logger from 'logger';
import GeneralErrorPage from './components/GeneralErrorPage';
import App from './App';

interface IAppContainerState {
  error: Error | null
}

class AppContainer extends PureComponent<{}, IAppContainerState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      error: null,
    };
  }

  public static getDerivedStateFromError(error: Error) {
    return {
      error,
    };
  }

  public render() {
    const { error } = this.state;
    if (error) {
      const id = uuid.v4();
      logger.error(error, { id });

      return (
        <GeneralErrorPage errorId={id} />
      );
    }

    return <App />;
  }
}

export default AppContainer;
