const isValidBigInteger = (value: unknown): value is bigint => {
  if (!value) {
    return false;
  }

  try {
    BigInt(value);

    return true;
  } catch (e) {
    return false;
  }
};

const isValidInterval = (value: unknown): value is number => {
  if (!value) {
    return false;
  }

  const val = Number(value);

  if (Number.isNaN(val) || !Number.isInteger(val)) {
    return false;
  }

  if (val < 1) {
    return false;
  }

  return true;
};

export {
  isValidBigInteger,
  isValidInterval,
};
