import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Empty, PageHeader, Table, Typography,
} from 'antd';

interface IRouteState {
  results?: [bigint, number][]
}

interface IOffboardingProps extends RouteComponentProps<{}, {}, IRouteState | undefined> {}

const Offboarding: React.FC<IOffboardingProps> = ({ history, location }) => {
  const results = location.state?.results ?? [];

  return (
    <>
      <PageHeader
        title="Home"
        onBack={() => history.push('/')}
      />
      {
        !results.length ? (
          <Empty />
        ) : (
          <>
            <Typography.Title>
              Results
            </Typography.Title>
            <Table
              columns={[
                {
                  key: 'number',
                  title: 'Number',
                  dataIndex: 'number',
                },
                {
                  key: 'count',
                  title: 'Count',
                  dataIndex: 'count',
                },
              ]}
              dataSource={
                results
                  .sort(
                    ([, aCount], [, bCount]) => (
                      bCount - aCount
                    ),
                  )
                  .map(
                    ([num, count]) => {
                      const strNumber = num.toString();

                      return {
                        key: strNumber,
                        number: strNumber,
                        count,
                      };
                    },
                  )
              }
            />
          </>
        )
      }
    </>
  );
};

Offboarding.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
  location: PropTypes.shape({
    state: PropTypes.shape({
      results: PropTypes.arrayOf(
        PropTypes.oneOfType(
          [PropTypes.any, PropTypes.number],
        ),
      ),
    }),
  }).isRequired,
} as React.ValidationMap<IOffboardingProps>;

export default Offboarding;
