import { PageHeader, Result, Typography } from 'antd';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import PropTypes from 'prop-types';

interface INotFoundPageProps extends RouteComponentProps {}

const NotFoundPage: React.FC<INotFoundPageProps> = ({ history }) => (
  <>
    <PageHeader
      title="Home"
      onBack={() => history.push('/')}
    />
    <Result
      status="404"
      title={(
        <Typography.Title>Page Not Found</Typography.Title>
        )}
      subTitle={(
        <>
          <Typography.Paragraph>
            This is unfortunate. The page you were looking for doesn&#39;t exist.
          </Typography.Paragraph>
          <Typography.Paragraph>
            If you think this page should exist, please contact the system administrator.
          </Typography.Paragraph>
        </>
        )}
    />
  </>
);

NotFoundPage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
  }).isRequired,
} as React.WeakValidationMap<INotFoundPageProps>;

export default NotFoundPage;
