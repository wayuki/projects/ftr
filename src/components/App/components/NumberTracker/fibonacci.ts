// Reference: https://en.wikipedia.org/wiki/Fibonacci_number#Recognizing_Fibonacci_numbers

const fibo = new Map<bigint, number>();
let n0 = BigInt('0');
let n1 = BigInt('1');
fibo.set(n0, 0);
fibo.set(n1, 1);

for (let i = 2; i < 1000; i += 1) {
  const tmp = n1;
  n1 = n0 + n1;
  n0 = tmp;

  fibo.set(n1, i);
}

const fibonacci = {
  numbers: fibo,
  check(n: bigint) {
    return this.numbers.has(n);
  },
};

export default fibonacci;
