import { message } from 'antd';
import { isValidInterval } from 'components/App/utils';
import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { qs } from 'utils';
import PropTypes from 'prop-types';

interface INumberTrackerContainerProps extends RouteComponentProps {}

const withContainer = <CompProps extends { interval: number }>(
  Comp: React.FC<CompProps>,
) => {
  const Container: React.FC<INumberTrackerContainerProps> = ({ history, location }) => {
    const { interval: rawInterval } = qs.parse<{ interval?: string }>(location.search);
    if (!isValidInterval(rawInterval)) {
      message.error(`Interval "${rawInterval}" is invalid`);

      history.replace('/');

      return null;
    }

    const interval = Number(rawInterval);

    const props = {
      interval,
    } as CompProps;

    return (
      <Comp
        {...props} // eslint-disable-line react/jsx-props-no-spreading
      />
    );
  };

  Container.propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
      replace: PropTypes.func.isRequired,
    }).isRequired,
    location: PropTypes.shape({
      search: PropTypes.string.isRequired,
    }).isRequired,
  } as React.ValidationMap<INumberTrackerContainerProps>;

  return Container;
};

export default withContainer;
