import {
  Button,
  Card, Col, Empty, Form, Input, message, PageHeader, Row, Space, Statistic, Table,
} from 'antd';
import classNames from 'classnames';
import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { qs } from 'utils';
import { isValidBigInteger } from 'components/App/utils';
import { useHistory } from 'react-router-dom';
import withContainer from './Container';
import style from './index.module.scss';
import useTimer from './timer';

interface INumberTrackerProps {
  interval: number
}

const BaseNumberTracker: React.FC<INumberTrackerProps> = ({ interval: intervalInSec }) => {
  const [form] = Form.useForm();
  const history = useHistory();

  const interval = intervalInSec * 10;

  const [number, setNumber] = useState<bigint | undefined>();

  const timer = useTimer(interval);

  const onTimerButtonClick = useCallback<React.MouseEventHandler<HTMLButtonElement>>(
    () => {
      if (timer.pause) {
        timer.resume();

        return;
      }

      timer.halt();
    },
    [timer.pause],
  );

  const onNumberChange = useCallback<React.ChangeEventHandler<HTMLInputElement>>(
    ({ target: { value } }) => {
      if (!isValidBigInteger(value)) {
        return;
      }

      const val = BigInt(value);

      setNumber(val);
    },
    [],
  );

  const onSubmit = useCallback(
    () => {
      if (number === undefined) {
        return;
      }

      if (timer.isValidFibonacci(number)) {
        message.info('FIB');
      }

      timer.record(number);

      setNumber(undefined);
      form.resetFields(['number']);
    },
    [form, number],
  );

  const onQuit = useCallback(
    () => {
      history.push('/results', {
        results: timer.results,
      });
    },
    [timer.results, history],
  );

  const seconds = Math.ceil(timer.timer / 10);

  return (
    <div className={style['number-tracker-container']}>
      <PageHeader
        title="Home"
        onBack={() => history.push(`/${qs.stringify({ interval: intervalInSec })}`)}
      />
      <Row gutter={[16, 16]}>
        <Col xs={{ span: 24 }} sm={{ span: 12 }}>
          <Card title="Timer" className={classNames(style.card, 'center')}>
            <Statistic
              className={style.statistic}
              value={seconds}
              suffix={seconds <= 1 ? 'second' : 'seconds'}
            />
            <Button onClick={onTimerButtonClick}>{timer.pause ? 'Resume' : 'Halt'}</Button>
          </Card>
        </Col>
        <Col xs={{ span: 24 }} sm={{ span: 12 }}>
          <Card title="Results" className={style.card}>
            {
              !Object.keys(timer.snapshot).length ? (
                <Empty description="Empty" />
              ) : (
                <Table
                  columns={[
                    {
                      key: 'number',
                      title: 'Number',
                      dataIndex: 'number',
                    },
                    {
                      key: 'count',
                      title: 'Count',
                      dataIndex: 'count',
                    },
                  ]}
                  dataSource={
                    timer.snapshot
                      .map(
                        ([num, count]) => {
                          const strNumber = num.toString();

                          return {
                            key: strNumber,
                            number: strNumber,
                            count,
                          };
                        },
                      )
                  }
                />
              )
            }
          </Card>
        </Col>
        <Col span={24}>
          <Form onFinish={onSubmit} form={form}>
            <Form.Item
              label="Number"
              name="number"
              rules={[
                {
                  required: true,
                  message: 'Number must be provided',
                },
                {
                  validator: async (_, value) => {
                    if (!value) {
                      return;
                    }

                    try {
                      BigInt(value);
                    } catch (e) {
                      throw new Error('Number must be an integer');
                    }
                  },
                },
              ]}
            >
              <Input
                type="number"
                placeholder="Please enter an integer"
                onChange={onNumberChange}
                allowClear
              />
            </Form.Item>
            <Form.Item>
              <Space>
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
                <Button type="default" htmlType="button" onClick={onQuit}>
                  Quit
                </Button>
              </Space>
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </div>
  );
};

BaseNumberTracker.propTypes = {
  interval: PropTypes.number.isRequired,
} as React.ValidationMap<INumberTrackerProps>;

const NumberTracker = withContainer(BaseNumberTracker);

export default NumberTracker;
