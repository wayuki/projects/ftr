import {
  useCallback, useEffect, useState,
} from 'react';
import fibonacci from './fibonacci';

interface IPrivateState {
  results: Map<bigint, number>
  snapshot: Map<bigint, number>
}

interface IState {
  pause: boolean
  timer: number
}

interface ITimer extends IState {
  results: [bigint, number][]
  snapshot: [bigint, number][]
  halt: () => void
  resume: () => void
  isValidFibonacci: (number: bigint) => boolean
  record: (number: bigint) => void
}

const useTimer = (interval: number): ITimer => {
  const [state, setState] = useState<IState & IPrivateState>({
    pause: false,
    timer: interval,
    results: new Map<bigint, number>(),
    snapshot: new Map<bigint, number>(),
  });

  useEffect(
    () => {
      const intv = setInterval(
        () => {
          setState(
            (prevState) => {
              if (prevState.pause) {
                return prevState;
              }

              let nextTimer = prevState.timer - 1;
              let nextSnapshot = prevState.snapshot;

              if (nextTimer <= 0) {
                nextTimer = interval;
                nextSnapshot = new Map(prevState.results);
              }

              return {
                ...prevState,
                timer: nextTimer,
                snapshot: nextSnapshot,
              };
            },
          );
        },
        100,
      );

      return () => clearInterval(intv);
    },
    [],
  );

  const halt = useCallback(
    () => {
      setState(
        (prevState) => ({
          ...prevState,
          pause: true,
        }),
      );
    },
    [],
  );

  const resume = useCallback(
    () => {
      setState(
        (prevState) => ({
          ...prevState,
          pause: false,
        }),
      );
    },
    [],
  );

  const isValidFibonacci = useCallback(
    (number: bigint) => fibonacci.check(number),
    [],
  );

  const record = useCallback(
    (number: bigint) => {
      setState(
        (prevState) => {
          const newResults = new Map(prevState.results);
          newResults.set(number, (newResults.get(number) || 0) + 1);

          return {
            ...prevState,
            results: newResults,
          };
        },
      );
    },
    [],
  );

  const results = Array.from(state.results.entries())
    .sort(
      ([, aCount], [, bCount]) => (
        bCount - aCount
      ),
    );

  const snapshot = Array.from(state.snapshot.entries())
    .sort(
      ([, aCount], [, bCount]) => (
        bCount - aCount
      ),
    );

  return {
    ...state,
    results,
    snapshot,
    halt,
    resume,
    isValidFibonacci,
    record,
  };
};

export default useTimer;
