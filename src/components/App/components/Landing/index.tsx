import {
  Button, Form, Input,
} from 'antd';
import React, { useCallback, useMemo } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { qs } from 'utils';
import PropTypes from 'prop-types';
import { isValidInterval } from 'components/App/utils';
import style from './index.module.scss';

interface ILandingProps extends RouteComponentProps {}

const DEFAULT_INTERVAL = 5;

const Landing: React.FC<ILandingProps> = ({ history, location }) => {
  const interval = useMemo(
    () => {
      const { interval: rawInterval } = qs.parse<{ interval?: string }>(location.search);
      if (!isValidInterval(rawInterval)) {
        history.replace(qs.stringify({ interval: DEFAULT_INTERVAL }));

        return DEFAULT_INTERVAL;
      }

      return Number(rawInterval);
    },
    [location.search],
  );

  const onSubmit = useCallback(
    () => {
      history.push(`/number-tracker${qs.stringify({ interval })}`);
    },
    [history, interval],
  );

  const onIntervalChange = useCallback<React.ChangeEventHandler<HTMLInputElement>>(
    ({ target: { value } }) => {
      if (!isValidInterval(value)) {
        return;
      }

      history.replace(qs.stringify({ interval: value }));
    },
    [],
  );

  return (
    <div className={style['landing-container']}>
      <Form
        onFinish={onSubmit}
      >
        <Form.Item
          label="Interval"
          name="interval"
          initialValue={interval}
          rules={[
            {
              required: true,
              message: 'Interval must be provided',
            },
            {
              validator: async (_, value) => {
                if (!value) {
                  return;
                }

                const val = Number(value);

                if (Number.isNaN(val) || !Number.isInteger(val)) {
                  throw new Error('Interval must be an integer');
                }

                if (val < 1) {
                  throw new Error('Interval must be larger than 0');
                }
              },
            },
          ]}
        >
          <Input
            type="number"
            placeholder="Number of Seconds"
            addonAfter={interval === 1 ? 'second' : 'seconds'}
            step={1}
            onChange={onIntervalChange}
          />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

Landing.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired,
    replace: PropTypes.func.isRequired,
  }).isRequired,
  location: PropTypes.shape({
    search: PropTypes.string.isRequired,
  }).isRequired,
} as React.WeakValidationMap<ILandingProps>;

export default Landing;
