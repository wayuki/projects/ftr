import { Button, Result, Typography } from 'antd';
import React from 'react';
import PropTypes from 'prop-types';

interface IGeneralErrorPageProps {
  errorId: string
}

const GeneralErrorPage: React.FC<IGeneralErrorPageProps> = ({ errorId }) => (
  <Result
    status="500"
    title={(
      <Typography.Title>Aww snap.</Typography.Title>
      )}
    subTitle={(
      <>
        <Typography.Paragraph>
          Sorry, we encountered an error.
        </Typography.Paragraph>
        <Typography.Paragraph>
          Please contact the system administrator with the following reference:
        </Typography.Paragraph>
        <Typography.Paragraph code>
          {errorId}
        </Typography.Paragraph>
        <Button
          onClick={() => window.location.reload()}
        >
          Click me to reload
        </Button>
      </>
      )}
  />
);

GeneralErrorPage.propTypes = {
  errorId: PropTypes.string.isRequired,
} as React.WeakValidationMap<IGeneralErrorPageProps>;

export default GeneralErrorPage;
