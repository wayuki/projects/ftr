import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';
import style from './index.module.scss';

const Container: React.FC = ({ children }) => (
  <>
    <div className={style.container}>
      <Typography.Paragraph>
        Source code is hosted on
        {' '}
        <a
          href="https://gitlab.com/wayuki/projects/ftr"
          target="_blank"
          rel="noreferrer"
        >
          https://gitlab.com/wayuki/projects/ftr
        </a>
        .
      </Typography.Paragraph>
      {children}
    </div>
  </>
);

Container.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]),
};

Container.defaultProps = {
  children: null,
};

export default Container;
