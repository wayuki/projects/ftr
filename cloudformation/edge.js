const regex = /\.[a-z0-9]+$/;
const indexDocument = 'index.html';

module.exports.handler = (event, context, callback) => {
  const {
    Records: {
      0: {
        cf: { request },
      },
    },
  } = event;

  if (request.uri.endsWith('/')) {
    callback(null, { ...request, uri: request.uri + indexDocument });
  } else if (request.uri.endsWith(indexDocument)) {
    callback(null, {
      status: 302,
      statusDescription: 'Found',
      headers: {
        location: [
          {
            key: 'Location',
            value: request.uri.substr(0, request.uri.length - indexDocument.length),
          },
        ],
      },
    });
  } else if (!regex.test(request.uri)) {
    callback(null, {
      status: 302,
      statusDescription: 'Found',
      headers: {
        location: [
          {
            key: 'Location',
            value: `${request.uri}/`,
          },
        ],
      },
    });
  } else {
    callback(null, request);
  }
};
